package com.amc.microservice.employeeservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.amc.microservice.employeeservice.bean.Employee;

@Repository
public interface EmployeeMongoRepository extends MongoRepository<Employee, Integer> {

}
