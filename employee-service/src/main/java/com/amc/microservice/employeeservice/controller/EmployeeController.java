package com.amc.microservice.employeeservice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amc.microservice.employeeservice.bean.Employee;
import com.amc.microservice.employeeservice.repository.EmployeeMongoRepository;
import com.amc.microservice.employeeservice.repository.EmployeeRepository;

@RestController
public class EmployeeController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmployeeRepository repository;

	@Autowired
	private EmployeeMongoRepository employeeMongoRepository;

	
	@GetMapping(value = "/employees")
	public @ResponseBody List<Employee> getEmployeeList() {
		logger.info("Get Employees invoked");
		return  employeeMongoRepository.findAll();
	}

	@GetMapping("/employee/id/{id}")
	public @ResponseBody Employee getEmployeeById(@PathVariable("id") int id) {
		logger.info("Get Employee with  id -> {} ", id);
		return repository.getEmployee(id);
	}

	@GetMapping("/employee/name/{name}")
	public @ResponseBody Employee getEmployeeByName(@PathVariable("name") String name) {
		logger.info("Get Employee with  name -> {} ", name);
		return repository.getEmployee(name);
	}

	@GetMapping("/employee/designation/{designation}")
	public @ResponseBody List<Employee> getEmployeeByDesignation(@PathVariable("designation") String designation) {
		logger.info("Get Employees with  designation -> {} ", designation);
		return repository.getEmployeeListByDesignation(designation);
	}

}
