package com.amc.microservice.employeeservice.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.amc.microservice.employeeservice.bean.Employee;

@Repository
public class EmployeeRepository {

	public List<Employee> getEmployeerList() {
		List<Employee> list = new ArrayList<Employee>();
		list.add(new Employee("CHK", 101, "SSE", "Infosys"));
		list.add(new Employee("ABC", 102, "TA", "TCS"));
		list.add(new Employee("DRT", 103, "TL", "TCS"));
		list.add(new Employee("SMX", 104, "STM", "Infosys"));
		list.add(new Employee("DTE", 105, "STM", "Infosys"));
		list.add(new Employee("BNH", 106, "SSE", "Infosys"));
		list.add(new Employee("YRT", 107, "SSE", "TCS"));
		list.add(new Employee("ERW", 108, "SSE", "EV"));
		list.add(new Employee("ACG", 109, "TA", "Infosys"));
		list.add(new Employee("ZFD", 110, "TA", "TCS"));
		list.add(new Employee("ERT", 111, "SSE", "EV"));
		list.add(new Employee("YTI", 112, "SSE", "EV"));
		list.add(new Employee("JDY", 113, "TL", "EV"));
		list.add(new Employee("PUI", 114, "SSE", "EV"));
		list.add(new Employee("RSA", 115, "SSE", "EV"));
		return list;
	}

	public Employee getEmployee(int id) {
		return getEmployeerList().stream().filter(employee -> employee.getEmployeeId() == id).findFirst()
				.orElseThrow(() -> new RuntimeException("Employee doesn't exist"));
	}

	public Employee getEmployee(String name) {
		return getEmployeerList().stream().filter(employee -> employee.getName().equals(name)).findFirst()
				.orElseThrow(() -> new RuntimeException("Employee doesn't exist"));
	}

	public Map<String, List<Employee>> getEmployeeGroupByDesignation() {
		return getEmployeerList().stream().collect(Collectors.groupingBy(Employee::getDesignation));
	}

	public List<Employee> getEmployeeListByDesignation(String designation) {
		return getEmployeerList().stream().filter(employee -> employee.getDesignation().equals(designation))
				.collect(Collectors.toList());
	}

	public List<Employee> getEmployeeListByCompany(String companyName) {
		return getEmployeerList().stream().filter(employee -> employee.getCompanyName().equals(companyName))
				.collect(Collectors.toList());
	}

}
