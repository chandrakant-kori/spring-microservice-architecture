# Spring Microservice Architecture
Microservice architecture implementation for Banking related services using Spring Cloud and Netflix. This project is a demo project to understand the how microservice architecture is being implemented. 

# Servers used to implement the microservice architecture 

| Server | Port | Description |
| ------- | ------- | ------- |
| `Spring Could Config Server` | 8888 | It is being used to configure the microservice environment and manage to zipkin distribution to trace the microservice using cloud bus |
| `Netflix Zuul API Gateway Server` | 8765 | It is being used as API Gateway to filter all the request call from one microservice to another microservice |
| `Netflix Eureka Naming Server` | 8761 | It is being used as naming server to register the microservice. |

# Services implemented 

| Service | Port | Description |
| ------- | ------- | ------- |
| `Banking Service` | 8000 | It is being used to serve banking services to the employee where an employee can have multiple accounts with different banks and it provides credit cards, open accounts, Demat related service by using other microservice proxies. |
| `Employee Service` | 8100 | It is being used to serve the employee-related information like employee name, customer id, company name, designation. |
| `Open Account Service` | 8200 | It is being used to provide open account services like FD to the employees via banking services |
| `Credit Card Service` | 8300 | It is being used to provide credit card services to the employees via banking services |
| `Demat Service` | 8400 | It is being used to provide trading and other digital investment services to the employees via banking service |
