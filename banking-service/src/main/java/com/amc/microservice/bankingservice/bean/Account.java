package com.amc.microservice.bankingservice.bean;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Account {

	@Id
	private ObjectId id;
	private String bankName;
	private String ifscCode;
	private int accountNo;
	private double amount;

	public Account() {
	}

	public Account(String bankName, String ifscCode, int accountNo, double amount) {
		this.bankName = bankName;
		this.ifscCode = ifscCode;
		this.accountNo = accountNo;
		this.amount = amount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public int getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
