package com.amc.microservice.bankingservice.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public class AccountRepository {

	public Map<Integer, List<Integer>> getCustomerList() {
		Map<Integer, List<Integer>> idAccountMapping = new HashMap<Integer, List<Integer>>();
		idAccountMapping.put(101, Arrays.asList(400001));
		idAccountMapping.put(102, Arrays.asList(5600001));
		idAccountMapping.put(103, Arrays.asList(7640001, 400008));
		idAccountMapping.put(104, Arrays.asList(7640002));
		idAccountMapping.put(105, Arrays.asList(400002));
		idAccountMapping.put(106, Arrays.asList(5600002, 7640004));
		idAccountMapping.put(107, Arrays.asList(400003, 7640006, 5600006));
		idAccountMapping.put(108, Arrays.asList(400004));
		idAccountMapping.put(109, Arrays.asList(5600003));
		idAccountMapping.put(110, Arrays.asList(400005, 7640005));
		idAccountMapping.put(111, Arrays.asList(5600004));
		idAccountMapping.put(112, Arrays.asList(400006));
		idAccountMapping.put(113, Arrays.asList(5600005));
		idAccountMapping.put(114, Arrays.asList(400007));
		idAccountMapping.put(115, Arrays.asList(7640003, 5600007));
		return idAccountMapping;
	}

	public List<Integer> getAccounts(int id) {
		List<Integer> accounts = new ArrayList<Integer>();
		getCustomerList().forEach((key, value) ->  {if (key == id)accounts.addAll(value);});
		return accounts;
	}

}
