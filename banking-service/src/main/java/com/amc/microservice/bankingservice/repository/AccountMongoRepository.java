package com.amc.microservice.bankingservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.amc.microservice.bankingservice.bean.Account;

@Repository
public interface AccountMongoRepository extends MongoRepository<Account, ObjectId> {

	public Account findByAccountNo(Integer accountNo);

}
