package com.amc.microservice.bankingservice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amc.microservice.bankingservice.bean.Account;
import com.amc.microservice.bankingservice.bean.Customer;
import com.amc.microservice.bankingservice.bean.CustomerAccount;
import com.amc.microservice.bankingservice.proxy.EmployeeServiceProxy;
import com.amc.microservice.bankingservice.repository.AccountRepository;
import com.amc.microservice.bankingservice.repository.AccountMongoRepository;

@RestController
public class BankingServiceController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Environment env;
	
	@Autowired
	private EmployeeServiceProxy employeeServiceProxy;

	@Autowired
	private AccountRepository customerRepository;

	@Autowired
	private AccountMongoRepository accountMongoRepository;

	@GetMapping("/customer/{id}")
	public CustomerAccount getCustomerAccountDetails(@PathVariable("id") int id) {

		logger.info("Inside getCustomer Account details with id -> {}", id);
		
		CustomerAccount accounts = new CustomerAccount();
		Customer customer = employeeServiceProxy.getCustommer(id);
		accounts.setCustomer(customer);
		customerRepository.getAccounts(id).forEach(accountNo -> accounts.getAccounts().add(accountMongoRepository.findByAccountNo(accountNo)));
		return accounts;

	}

	@GetMapping("/accounts")
	public List<Account> getCustomerAccountDetails() {

		logger.info("Inside getCustomer Account details on port -> {}",env.getProperty("server.port"));
		logger.info("Inside accounts");
		return accountMongoRepository.findAll();

	}
}
