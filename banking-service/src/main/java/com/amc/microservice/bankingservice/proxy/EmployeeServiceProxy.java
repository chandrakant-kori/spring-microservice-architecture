package com.amc.microservice.bankingservice.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.amc.microservice.bankingservice.bean.Customer;

@FeignClient(name = "netflix-zuul-api-gateway-server")
@RibbonClient(name = "employee-service")
public interface EmployeeServiceProxy {

	@GetMapping("/employee-service/employee/id/{id}")
	public Customer getCustommer(@PathVariable("id") int id);

}
